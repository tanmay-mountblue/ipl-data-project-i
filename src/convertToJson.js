let csvToJson = require("convert-csv-to-json");
const path = require("path");

const deliveriesInputName = path.join(__dirname, "./data/deliveries.csv");
const matchesInputName = path.join(__dirname, "./data/matches.csv");

const deliveriesOutputName = path.join(__dirname, "./data/deliveries.json");
const matchesOutputName = path.join(__dirname, "./data/matches.json");
 
csvToJson
  .fieldDelimiter(",")
  .generateJsonFileFromCsv(deliveriesInputName, deliveriesOutputName);
csvToJson
  .fieldDelimiter(",")
  .generateJsonFileFromCsv(matchesInputName, matchesOutputName);
