/***
 * Chart for MatchesPerYear
 */
function problem1() {
  let chartData1;
  fetch("./output/matchPerYear.json")
    .then((response) => response.json())
    .then((data) => {
      let arr = [];
      for (key in data) {
        arr.push([key, data[key]]);
      }

      /**
       * Adding high chart js
       */
      Highcharts.chart("chart1", {
        chart: {
          type: "column",
        },
        title: {
          text: "Matches Held Per Year In IPL",
        },
        subtitle: {
          text: 'Source: <a href="http://en.wikipedia.org/wiki/List_of_cities_proper_by_population">Wikipedia</a>',
        },
        xAxis: {
          type: "category",
          labels: {
            rotation: -45,
            style: {
              fontSize: "13px",
              fontFamily: "Verdana, sans-serif",
            },
          },
        },
        yAxis: {
          min: 0,
          title: {
            text: "Matches (Numbers)",
          },
        },
        legend: {
          enabled: false,
        },
        tooltip: {
          pointFormat: "Matches: <b>{point.y:.1f}</b>",
        },
        series: [
          {
            name: "Matches",
            data: arr,
            dataLabels: {
              enabled: true,
              rotation: -90,
              color: "#FFFFFF",
              align: "right",
              format: "{point.y:.1f}", // one decimal
              y: 10, // 10 pixels down from the top
              style: {
                fontSize: "13px",
                fontFamily: "Verdana, sans-serif",
              },
            },
          },
        ],
      });
    });
}

/***
 * Matches Won Per Team Per Year Chart (Problem 2)
 */
function problem2() {
  let chartData2;
  fetch("./output/matchesWonPerTeamPerYear.json")
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
      let teams = Object.keys(data);
      console.log(teams);
      let series = [];
      let distinctSeasons = [];
      for (team in data) {
        for (season in data[team]) {
          if (!distinctSeasons.find((s) => s === season)) {
            distinctSeasons.push(season);
          }
        }
      }
      distinctSeasons.sort();
      console.log(distinctSeasons);
      distinctSeasons.forEach((season) => {
        update(season);
      });
      function update(season) {
        let tempObj = {};
        tempObj["name"] = season;
        tempObj["data"] = [];
        teams.forEach((team) => {
          if (data[team].hasOwnProperty(season)) {
            tempObj.data.push(data[team][season]);
          } else {
            tempObj.data.push(0);
          }
        });
        series.push(tempObj);
      }
      console.log(series);

      /**
       * Adding high chart js
       */
      Highcharts.chart("chart2", {
        chart: {
          type: "column",
        },

        title: {
          text: "Matches Won Per Team Per Season",
        },

        xAxis: {
          categories: teams,
        },

        yAxis: {
          allowDecimals: false,
          min: 0,
          title: {
            text: "Number of Matches won By Team",
          },
        },

        tooltip: {
          formatter: function () {
            return (
              "<b>" +
              this.x +
              "</b><br/>" +
              this.series.name +
              ": " +
              this.y +
              "<br/>" +
              "Total: " +
              this.point.stackTotal
            );
          },
        },

        plotOptions: {
          column: {
            stacking: "normal",
          },
        },

        series: series,
      });
    });
}
problem1();
problem2();
/***
 * Chart for MatchesPerYear
 */
function problem3() {
  let chartData1;
  fetch("./output/extraRunsConcededPerTeam2016.json")
    .then((response) => response.json())
    .then((data) => {
      let arr = [];
      for (key in data) {
        let tempObj = {};
        tempObj["name"] = key;
        tempObj["y"] = data[key];
        arr.push(tempObj);
      }

      /**
       * Adding high chart js
       */
      Highcharts.chart("chart3", {
        chart: {
          plotBackgroundColor: null,
          plotBorderWidth: null,
          plotShadow: false,
          type: "pie",
        },
        title: {
          text: "Extra Runs Conceded by Teams in 2016",
        },
        tooltip: {
          pointFormat: "{series.name}: <b>{point.percentage:.1f}%</b>",
        },
        accessibility: {
          point: {
            valueSuffix: "%",
          },
        },
        plotOptions: {
          pie: {
            allowPointSelect: true,
            cursor: "pointer",
            dataLabels: {
              enabled: true,
              format: "<b>{point.name}</b>: {point.percentage:.1f} %",
            },
          },
        },
        series: [
          {
            name: "Extra Runs",
            colorByPoint: true,
            data: arr,
          },
        ],
      });
    });
}
problem3();
