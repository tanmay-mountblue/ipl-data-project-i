const { forIn, max } = require("lodash");

/**
 * Problem 1
 */
exports.teamWonTossAlsoWonMatch = (matchesData) => {
  // teamWonTossAlsoWonMatchCount = {team1:number,team2:number2,...}
  /**
   * Loop through matchesdata to count number of times each team won toss and also the match
   */
  let teamWonTossAlsoWonMatchCount = matchesData.reduce(
    (teamWonTossAlsoWonMatchCount, matchData) => {
      if (matchData.toss_winner === matchData.winner) {
        if (teamWonTossAlsoWonMatchCount.hasOwnProperty(matchData.winner)) {
          teamWonTossAlsoWonMatchCount[matchData.winner] += 1;
        } else {
          teamWonTossAlsoWonMatchCount[matchData.winner] = 1;
        }
      }
      return teamWonTossAlsoWonMatchCount;
    },
    {}
  );
  return teamWonTossAlsoWonMatchCount;
};

//
// player who has won the highest number of Player of the Match awards for each season
//
exports.playersWithHighestNumberOfPlayerOfTheMatchAwards = (matchesData) => {
  /**
   * Calculate Distinct Seasons
   */
  let distinctIPLSeasons = matchesData.reduce(
    (distinctIPLSeasons, matchData) => {
      if (!distinctIPLSeasons.includes(matchData.season)) {
        distinctIPLSeasons.push(matchData.season);
      }
      return distinctIPLSeasons;
    },
    []
  );

  /**
   * Call function countManOfMatches to get player with maximum awards in particular year
   */
  let playersWithHighestNumberOfPlayerOfTheMatchAwardsCount =
    distinctIPLSeasons.reduce(
      (playersWithHighestNumberOfPlayerOfTheMatchAwardsCount, season) => {
        playersWithHighestNumberOfPlayerOfTheMatchAwardsCount[season] =
          countManOfMatches(season, matchesData);
        return playersWithHighestNumberOfPlayerOfTheMatchAwardsCount;
      },
      {}
    );

  return playersWithHighestNumberOfPlayerOfTheMatchAwardsCount;

  /**
   * Function to calculate man of matches awarded to each player in each season
   */
  function countManOfMatches(season, matchesData) {
    let playerOfMatches = matchesData.reduce((playerOfMatches, matchData) => {
      if (matchData.season === season)
        if (playerOfMatches.hasOwnProperty(matchData.player_of_match)) {
          playerOfMatches[matchData.player_of_match] += 1;
        } else {
          playerOfMatches[matchData.player_of_match] = 1;
        }
      return playerOfMatches;
    }, {});
    let max = 0,
      playerWithMaxAwards = "";
    for (player in playerOfMatches) {
      if (playerOfMatches[player] > max) {
        max = playerOfMatches[player];
        playerWithMaxAwards = player;
      }
    }
    return playerWithMaxAwards;
  }
};

/**
 * Problem 3
 */
exports.strikeRateOfBatsmanForEachSeason = (matchesData, deliveriesData) => {
  //{batsmam1:{"season1":strikerate1,...},...}
  let distinctIPLSeasons = matchesData.reduce(
    (distinctIPLSeasons, matchData) => {
      if (!distinctIPLSeasons.includes(matchData.season)) {
        distinctIPLSeasons.push(matchData.season);
      }
      return distinctIPLSeasons;
    },
    []
  );

  let distinctIPLBatsmen = deliveriesData.reduce(
    (distinctIPLBatsmen, deliveryData) => {
      if (!distinctIPLBatsmen.includes(deliveryData.batsman)) {
        distinctIPLBatsmen.push(deliveryData.batsman);
      }
      return distinctIPLBatsmen;
    },
    []
  );
  /***
   * Getting delivery data for each season
   */
  let deliveryDataForEachSeason = distinctIPLSeasons.reduce(
    (deliveryDataForEachSeason, season) => {
      deliveryDataForEachSeason[season] = getDeliveryDataForSeason(
        season,
        matchesData,
        deliveriesData
      );
      return deliveryDataForEachSeason;
    },
    {}
  );
  let strikeRateOfBatsmanForEachSeasonObj = distinctIPLBatsmen.reduce(
    (strikeRateOfBatsmanForEachSeasonObj, batsman) => {
      let tempObj = {};
      for (season in deliveryDataForEachSeason) {
        let bowls = 0;
        let seasonRuns = deliveryDataForEachSeason[season].reduce(
          (seasonRuns, deliveryData) => {
            if (deliveryData.batsman === batsman) {
              seasonRuns += +deliveryData.batsman_runs;
              bowls++;
            }
            return seasonRuns;
          },
          0
        );
        if (bowls > 0) {
          let strikeRate = (seasonRuns / bowls) * 100;
          tempObj[season] = strikeRate;
        }
      }
      strikeRateOfBatsmanForEachSeasonObj[batsman] = tempObj;
      return strikeRateOfBatsmanForEachSeasonObj;
    },
    {}
  );
  return strikeRateOfBatsmanForEachSeasonObj;

  /***
   * Function to get delivery data the season
   */
  function getDeliveryDataForSeason(season, matchesData, deliveriesData) {
    let matchesdataForSeason = matchesData.filter((matchData) => {
      if (matchData.season === season) {
        return matchData;
      }
    });

    let deliveriesDataForSeason = deliveriesData.filter((deliveryData) => {
      if (
        matchesdataForSeason.find((matchData) => {
          if (matchData.id === deliveryData.match_id) {
            return 1;
          }
        })
      ) {
        return deliveryData;
      }
    });

    return deliveriesDataForSeason;
  }
};

/**
 * Problem 4
 */
exports.hisghestNumTimesPlayerDismissedByAnotherPlayer = (
  matchesData,
  deliveriesData
) => {
  let deliveriesWithDismissals = deliveriesData.reduce(
    (deliveriesWithDismissals, deliveryData) => {
      if (deliveryData.player_dismissed.length) {
        deliveriesWithDismissals.push(deliveryData);
      }
      return deliveriesWithDismissals;
    },
    []
  );
  let distinctIPLBatsmen = deliveriesWithDismissals.reduce(
    (distinctIPLBatsmen, deliveryData) => {
      if (!distinctIPLBatsmen.includes(deliveryData.batsman)) {
        distinctIPLBatsmen.push(deliveryData.batsman);
      }
      return distinctIPLBatsmen;
    },
    []
  );
  let hisghestNumTimesPlayerDismissedByAnotherPlayerObj =
    distinctIPLBatsmen.reduce(
      (hisghestNumTimesPlayerDismissedByAnotherPlayerObj, batsman) => {
        hisghestNumTimesPlayerDismissedByAnotherPlayerObj[batsman] =
          calculateDismissalsByBowlers(batsman, deliveriesWithDismissals);
        return hisghestNumTimesPlayerDismissedByAnotherPlayerObj;
      },
      {}
    );
  let max = 0,
    obj = {};
  for (batsman in hisghestNumTimesPlayerDismissedByAnotherPlayerObj) {
    let arr = Object.keys(
      hisghestNumTimesPlayerDismissedByAnotherPlayerObj[batsman]
    );
    key = arr[0];
    if (hisghestNumTimesPlayerDismissedByAnotherPlayerObj[batsman][key] > max) {
      max = hisghestNumTimesPlayerDismissedByAnotherPlayerObj[batsman][key];
      obj = {};
      obj[batsman] = hisghestNumTimesPlayerDismissedByAnotherPlayerObj[batsman];
    }
  }

  return obj;

  function calculateDismissalsByBowlers(batsman, deliveriesWithDismissals) {
    let tempObj = {};
    let batsmansDismissals = deliveriesWithDismissals.reduce(
      (batsmansDismissals, deliveryData) => {
        if (deliveryData.player_dismissed === batsman) {
          batsmansDismissals.push(deliveryData);
        }
        return batsmansDismissals;
      },
      []
    );
    tempObj = batsmansDismissals.reduce((tempObj, deliveryData) => {
      if (tempObj.hasOwnProperty(deliveryData.bowler)) {
        tempObj[deliveryData.bowler] += 1;
      } else {
        tempObj[deliveryData.bowler] = 1;
      }
      return tempObj;
    }, {});
    let max = 0,
      obj = {};
    for (bowler in tempObj) {
      if (tempObj[bowler] > max) {
        max = tempObj[bowler];
        obj = {};
        obj[bowler] = max;
      }
    }
    return obj;
  }
};

/**
 * Problem 5
 */
exports.bowlerWithBestEconomyInSuperOvers = (matchesData, deliveriesData) => {
  let bowlersEconomy = [];

  let deliveriesWithSuperOver = deliveriesData.reduce(
    (deliveriesWithSuperOver, deliveryData) => {
      if (deliveryData.is_super_over !== "0") {
        deliveriesWithSuperOver.push(deliveryData);
      }
      return deliveriesWithSuperOver;
    },
    []
  );
  let distinctBlowlers = deliveriesWithSuperOver.reduce(
    (distinctBlowlers, deliveryData) => {
      if (!distinctBlowlers.includes(deliveryData.bowler)) {
        distinctBlowlers.push(deliveryData.bowler);
      }
      return distinctBlowlers;
    },
    []
  );

  /**
   * Function calls to calculate economy for each bowler
   */
  distinctBlowlers.forEach((bowler) => {
    let economy = calculateEconomy(bowler, deliveriesWithSuperOver);
    let tempObj = {};

    tempObj[bowler] = economy;

    bowlersEconomy.push(tempObj);
  });

  /**
   * Bubble sort
   */
  function swap(arr, xp, yp) {
    var temp = arr[xp];
    arr[xp] = arr[yp];
    arr[yp] = temp;
  }
  var i, j;
  let n = bowlersEconomy.length;
  for (i = 0; i < n - 1; i++) {
    for (j = 0; j < n - i - 1; j++) {
      let keys1 = Object.keys(bowlersEconomy[j]);
      let keys2 = Object.keys(bowlersEconomy[j + 1]);

      if (bowlersEconomy[j][keys1[0]] > bowlersEconomy[j + 1][keys2[0]]) {
        swap(bowlersEconomy, j, j + 1);
      }
    }
  }

  return bowlersEconomy[0];

  /**
   * Function to calculate particular players economy
   */
  function calculateEconomy(player, deliveriesWithSuperOver) {
    /***
     * Filter deliveries data for player
     */
    const playerDeliveriesInSuperOver = deliveriesWithSuperOver.filter(
      (deliveryWithSuperOver) => {
        if (deliveryWithSuperOver.bowler === player) {
          return deliveryWithSuperOver;
        }
      }
    );

    /**
     * Calculate runs conceded by player
     */
    const totalRunsConceded = playerDeliveriesInSuperOver.reduce(
      (totalRunsConceded, playerDeliveryInSuperOver) => {
        totalRunsConceded += +playerDeliveryInSuperOver.total_runs;
        return totalRunsConceded;
      },
      0
    );
    /**
     * Calculate overs bowled by player
     */
    const totalOversBowled = playerDeliveriesInSuperOver.reduce(
      (totalOversBowled, playerDeliveryInSuperOver) => {
        if (
          !totalOversBowled.includes(
            playerDeliveryInSuperOver.match_id + playerDeliveryInSuperOver.over
          )
        ) {
          totalOversBowled.push(
            playerDeliveryInSuperOver.match_id + playerDeliveryInSuperOver.over
          );
        }
        return totalOversBowled;
      },
      []
    );

    /**
     * Calculate Economy
     */
    return totalRunsConceded / totalOversBowled.length;
  }
};
