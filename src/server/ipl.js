var _ = require("lodash");
/***
 * Problem 1
 */
exports.matchPerYear = (matchesData) => {
  const matchCount = matchesData.reduce((matchCount, currentMatchData) => {
    if (matchCount[currentMatchData.season]) {
      matchCount[currentMatchData.season] += 1;
    } else {
      matchCount[currentMatchData.season] = 1;
    }
    return matchCount;
  }, {});

  return matchCount;

  /**
   * Using Lodash
   */
  //return  _.countBy(matchesData, 'season');
};

/***
 * Problem 2
 */
exports.matchesWonPerTeamPerYear = (matches) => {
  //[{“team1”: [{“year1”:”matches won”},{“year2”:”matches won”}]},{“team2”: [{“year1”:”matches won”},{“year2”:”matches won”}]}]
  let outputObj = {};
  matches.forEach((matchData) => {
    if (outputObj.hasOwnProperty(matchData.winner)) {
      if (outputObj[matchData.winner].hasOwnProperty(matchData.season)) {
        outputObj[matchData.winner][matchData.season] += 1;
      } else {
        outputObj[matchData.winner][matchData.season] = 1;
      }
    } else {
      outputObj[matchData.winner] = {};
      outputObj[matchData.winner][matchData.season] = 1;
    }
  });
  return outputObj;
};

/***
 * Problem 3
 */
exports.extraRunsConcededPerTeam2016 = (matchesData, deliveriesData) => {
  let matches2016 = matchesData.filter((matchData) => {
    if (matchData.season === "2015") {
      return matchData.id;
    }
  });

  let extraRunsConcededCount = deliveriesData
    .filter((deliveryData) => {
      if (
        deliveryData.extra_runs != 0 &&
        matches2016.find((matchData2016) => {
          if (matchData2016.id === deliveryData.match_id) {
            return 1;
          }
        })
      ) {
        return deliveryData;
      }
    })
    .reduce((extraRunsConcededCount, deliveryData2016) => {
      if (
        extraRunsConcededCount.hasOwnProperty(deliveryData2016.bowling_team)
      ) {
        extraRunsConcededCount[deliveryData2016.bowling_team] += parseInt(
          deliveryData2016.extra_runs
        );
      } else {
        extraRunsConcededCount[deliveryData2016.bowling_team] = parseInt(
          deliveryData2016.extra_runs
        );
      }
      return extraRunsConcededCount;
    }, {});

  return extraRunsConcededCount;
};

/***
 * Problem 4
 */
exports.topTenEconomicalBowlers2015 = (matchesData, deliveriesData) => {
  let topTenEcoBowlers2015 = [];
  let bowlersEconomy = [];

  /**
   * Filter matches of 2015
   */
  const matches2015 = matchesData.filter((matchData) => {
    if (matchData.season === "2015") {
      return matchData;
    }
  });

  /**
   * Filter Deliveries of 2015
   */
  let deliveriesData2015 = deliveriesData.filter((deliveryData) => {
    if (
      matches2015.find((matchData2015) => {
        if (matchData2015.id === deliveryData.match_id) {
          return 1;
        }
      })
    ) {
      return deliveryData;
    }
  });

  /**
   * Loop to calculate distinct bowler names
   */
  const bowlers = deliveriesData2015.reduce((bowlers, deliveryData2015) => {
    if (!bowlers.includes(deliveryData2015.bowler)) {
      bowlers.push(deliveryData2015.bowler);
    }
    return bowlers;
  }, []);

  /**
   * Function calls to calculate economy for each bowler
   */
  bowlers.forEach((bowler) => {
    let economy = calculateEconomy(bowler, deliveriesData2015);
    let tempObj = {};

    tempObj[bowler] = economy;

    bowlersEconomy.push(tempObj);
  });

  /**
   * Bubble sort
   */
  function swap(arr, xp, yp) {
    var temp = arr[xp];
    arr[xp] = arr[yp];
    arr[yp] = temp;
  }
  var i, j;
  let n = bowlersEconomy.length;
  for (i = 0; i < n - 1; i++) {
    for (j = 0; j < n - i - 1; j++) {
      let keys1 = Object.keys(bowlersEconomy[j]);
      let keys2 = Object.keys(bowlersEconomy[j + 1]);

      if (bowlersEconomy[j][keys1[0]] > bowlersEconomy[j + 1][keys2[0]]) {
        swap(bowlersEconomy, j, j + 1);
      }
    }
  }

  /**
   * get top ten bowlers
   */
  topTenEcoBowlers2015 = bowlersEconomy.slice(0, 10);

  return topTenEcoBowlers2015;

  /**
   * Function to calculate particular players economy
   */
  function calculateEconomy(player, deliveriesData2015) {
    /***
     * Filter deliveries data for player
     */
    const playerDeliveriesData2015 = deliveriesData2015.filter(
      (deliveryData2015) => {
        if (deliveryData2015.bowler === player) {
          return deliveryData2015;
        }
      }
    );

    /**
     * Calculate runs conceded by player
     */
    const totalRunsConceded = playerDeliveriesData2015.reduce(
      (totalRunsConceded, playerDeliveryData2015) => {
        totalRunsConceded += +playerDeliveryData2015.total_runs;
        return totalRunsConceded;
      },
      0
    );
    /**
     * Calculate overs bowled by player
     */
    const totalOversBowled = playerDeliveriesData2015.reduce(
      (totalOversBowled, playerDeliveryData2015) => {
        if (
          !totalOversBowled.includes(
            playerDeliveryData2015.match_id + playerDeliveryData2015.over
          )
        ) {
          totalOversBowled.push(
            playerDeliveryData2015.match_id + playerDeliveryData2015.over
          );
        }
        return totalOversBowled;
      },
      []
    );

    /**
     * Calculate Economy
     */
    return totalRunsConceded / totalOversBowled.length;
  }
};
