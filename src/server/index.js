const path = require("path");
const fs = require("fs");
const {
  matchPerYear,
  matchesWonPerTeamPerYear,
  extraRunsConcededPerTeam2016,
  topTenEconomicalBowlers2015
} = require("./ipl");
const {
  teamWonTossAlsoWonMatch,
  playersWithHighestNumberOfPlayerOfTheMatchAwards,
  strikeRateOfBatsmanForEachSeason,
  bowlerWithBestEconomyInSuperOvers,
  hisghestNumTimesPlayerDismissedByAnotherPlayer,
} = require("./AdditionalIPL.js");
/**
 * Read the data from JSON files
 */

const deliveriesData = require(path.join(__dirname, "../data/deliveries.json"));
const matchesData = require(path.join(__dirname, "../data/matches.json"));

/**
 * Higher order function : to run ipl functons
 */

const executeIplFunction = (
  matchesData,
  deliveriesData,
  outFileName,
  iplCb
) => {
  const iplResult = iplCb(matchesData, deliveriesData);
  //console.log(iplResult);
  const outFilePath = path.join(
    __dirname,
    `../public/output/${outFileName}.json`
  );
  fs.writeFile(outFilePath, JSON.stringify(iplResult), (err, data) => {
    if (err) throw err;
    console.log("complete");
  });
};

/**
 * Run IPL functions
 */
executeIplFunction(matchesData, deliveriesData, "matchPerYear", matchPerYear);
executeIplFunction(
  matchesData,
  deliveriesData,
  "matchesWonPerTeamPerYear",
  matchesWonPerTeamPerYear
);
executeIplFunction(
  matchesData,
  deliveriesData,
  "extraRunsConcededPerTeam2016",
  extraRunsConcededPerTeam2016
);
executeIplFunction(
  matchesData,
  deliveriesData,
  "topTenEconomicalBowlers2015",
  topTenEconomicalBowlers2015
);

/**
 * Run Additional IPL functions
 */
executeIplFunction(
  matchesData,
  [],
  "teamWonTossAlsoWonMatch",
  teamWonTossAlsoWonMatch
);
executeIplFunction(
  matchesData,
  [],
  "playersWithHighestNumberOfPlayerOfTheMatchAwards",
  playersWithHighestNumberOfPlayerOfTheMatchAwards
);
executeIplFunction(
  [],
  deliveriesData,
  "bowlerWithBestEconomyInSuperOvers",
  bowlerWithBestEconomyInSuperOvers
);
executeIplFunction(
  [],
  deliveriesData,
  "hisghestNumTimesPlayerDismissedByAnotherPlayer",
  hisghestNumTimesPlayerDismissedByAnotherPlayer
);
executeIplFunction(
  matchesData,
  deliveriesData,
  "strikeRateOfBatsmanForEachSeason",strikeRateOfBatsmanForEachSeason,
  strikeRateOfBatsmanForEachSeason
);
